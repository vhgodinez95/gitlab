export default () => ({
  packageEntity: null,
  packageFiles: [],
  pipelineInfo: {},
  pipelineError: null,
  isLoading: false,
});
