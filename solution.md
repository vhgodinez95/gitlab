GitLab can do that for you with no pain! Yay! What you need to do is very simple: enable GitLab Repository Mirroring!
More info in: https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html
